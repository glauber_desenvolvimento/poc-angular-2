import { ContatosModule } from './../clientes/clientes.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [//Components
    AppComponent
  ],
  imports: [//Modules
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    //CHILDS MODULES
    ContatosModule
  ],
  exports: [ //Components
  ],
  providers: [//Services

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
