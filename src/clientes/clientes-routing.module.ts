import { ClientesComponent } from './clientes.component';
import { ClienteDetalheComponent } from './cliente-detalhe.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const clientesRoutes: Routes = [
    {
        path: 'clientes',
        component: ClientesComponent
    },
    {
        path: 'cliente/novo',
        component: ClienteDetalheComponent
    },
    {
        path: 'cliente/:id',
        component: ClienteDetalheComponent
    }
];

@NgModule({
    imports:[
        RouterModule.forChild(clientesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ClientesRoutingModule{}