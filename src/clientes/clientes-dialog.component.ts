import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'clientes-dialog',
    templateUrl: './clientes-dialog.component.html',
  })
  export class ClientesDialogComponent {
  
    constructor(
      public dialogRef: MatDialogRef<ClientesDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data
    ) {}
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }
  