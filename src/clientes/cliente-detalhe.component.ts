import { ClienteService } from './cliente.service';
import { ClienteModel } from './cliente.model';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
//   moduleId: module.id,
  templateUrl: "cliente-detalhe.component.html",
  selector: 'cliente-detalhe',
  styleUrls: ['./cliente-detalhe.component.scss', '../app/app.component.scss']
})
export class ClienteDetalheComponent implements OnInit {
  cliente = new ClienteModel();
  constructor(
    private route: ActivatedRoute, 
    private location: Location,
    private clienteService: ClienteService
  ) { }

  id: number = 0;
  private getId():Promise<number>{
    this.route.params.forEach((params: Params) => {
      this.id = (params['id']) ? +params['id'] : 0;
    });
    return Promise.resolve(this.id);
  }


  ngOnInit():void {

    this.getId().then((id) => {
      if(id){
        this.clienteService.getById(id)
        .then((cliente) => {
            this.cliente = JSON.parse(JSON.stringify(cliente))
        });
      }
    });

  }
  onSubmit(event){
    event.preventDefault();
    if(!this.id){
      this.clienteService.create(this.cliente).then(cliente => {
        this.goback()
      });
    }else{
      this.clienteService.update(this.cliente).then(cliente => {
        this.goback()
      });
    }
  }

  goback(event?): void{
    if(event){
      event.preventDefault();
    }
    this.location.back();
  }

}
