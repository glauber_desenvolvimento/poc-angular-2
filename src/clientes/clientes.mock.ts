import { ClienteModel } from './cliente.model';
export const CLIENTES: ClienteModel[] = [
    new ClienteModel("Fulano", "fulano@gmail.com", "9999-9999", 1),
    new ClienteModel("Beltrano", "beltrano@gmail.com", "9999-9999", 2),
    new ClienteModel("Ciclano", "ciclano@gmail.com", "9999-9999", 3)
];