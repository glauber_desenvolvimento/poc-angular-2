import { ClienteService } from './cliente.service';
import { ClientesDialogComponent } from './clientes-dialog.component';
import { ClientesComponent } from './clientes.component';
import { ClienteDetalheComponent } from './cliente-detalhe.component';
import { ClientesRoutingModule } from './clientes-routing.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule}  from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [ //Components
    ClientesComponent,
    ClienteDetalheComponent,
    ClientesDialogComponent
  ], 
  imports: [
    CommonModule, 
    ClientesRoutingModule, 
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule
  ],
  exports: [ //Components
    ClientesComponent
  ],
  providers: [ //Services
    ClienteService
  ]
})
export class ContatosModule {
}
