export class ClienteModel{
    id: number;
    nome: string;
    email: string;
    celular: string;
    constructor(nome="", email="", celular="", id=0){
        this.nome = nome;
        this.email = email;
        this.celular = celular;
        if(id){
            this.id = id;
        }
    }
}