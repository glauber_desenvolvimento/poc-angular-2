import { ClienteModel } from './cliente.model';
import { ClienteService } from './cliente.service';
import { ClientesDialogComponent } from './clientes-dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
//   moduleId: module.id,
  templateUrl: "clientes.component.html",
  selector: 'clientes',
  styleUrls: ['./clientes.component.scss', '../app/app.component.scss']
})
export class ClientesComponent implements OnInit {

  clientes:ClienteModel[] = [];
  displayedColumns = ['id', 'nome', 'celular', 'email', 'acoes'];
  timeout;

  constructor(
    public dialog: MatDialog,
    private clienteService: ClienteService
  ) {
    this.timeout = setTimeout(()=>{}, 10);
    console.log(this.timeout);
  }

  ngOnInit():void {
    this.clienteService.getAll().then((clientes) => {
      this.clientes = clientes;
    });
  }

  deletar(item):void {
    const dialogRef = this.dialog.open(ClientesDialogComponent, {
      width: '301px',
      data: {
        message: "Deseja excluir este registro?", 
        item: item
      }
    });
    dialogRef.afterClosed().subscribe(
      response => {
        if(response){

        }
      }
    );
  }

  pesquisar(event){
    if(event.target.value){
      clearTimeout(this.timeout);
      this.timeout = setTimeout(()=>{
        this.clienteService.search(event.target.value).then((clientes) => {
          this.clientes = clientes;
        });
      }, 300);
    }else{
      this.clienteService.getAll().then((clientes) => {
        this.clientes = clientes;
      });
    }
  }

}

