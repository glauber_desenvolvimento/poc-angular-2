import { Injectable } from '@angular/core';
import { ClienteModel } from './cliente.model';
import { CLIENTES } from './clientes.mock';
// import { Http, Headers, Response } from "@angular/http";
// import 'rxjs/add/operator/toPromise';

@Injectable()
export class ClienteService{
    // private origin: string = "";
    // private headers: Headers = new Headers({'Content-Type': 'application/json'});
    clientes: ClienteModel[];
    constructor(
        // private http: Http
    ){
        this.clientes = CLIENTES;
    }

    getAll(): Promise<ClienteModel[]>{
        // return this.http.get(this.origin)
        //     .toPromise()
        //     .then( response => response.json().data as ClienteModel[] )
        //     .catch( this.handleError );
        return Promise.resolve(this.clientes);
    }

    create(cliente: ClienteModel): Promise<ClienteModel>{
        // return this.http
        //     .post(this.origin, JSON.stringify(contato), {headers: this.headers})
        //     .toPromise()
        //     .then((response: Response) => response.json().data as Contato)
        //     .catch(this.handleError);
        cliente.id = this.clientes.length+1;
        this.clientes.push(cliente)
        return Promise.resolve(cliente);
    }

    update(cliente: ClienteModel): Promise<ClienteModel>{
        // const url = `${this.origin}/${cliente.id}`; // app/contatos/:id
        // return this.http
        //     .put(url, JSON.stringify(contato), {headers: this.headers})
        //     .toPromise()
        //     .then(() => contato as Contato)
        //     .catch(this.handleError);
        this.clientes.forEach((item, i) =>{
            if(item.id == cliente.id){
                this.clientes[i] = cliente;
            }
        });
        return Promise.resolve(cliente);
    }

    delete(cliente: ClienteModel): Promise<ClienteModel> {
        // const url = `${this.origin}/${cliente.id}`; // app/contatos/:id
        // return this.http
        //     .delete(url, {headers: this.headers})
        //     .toPromise()
        //     .then( () => contato as Contato)
        //     .catch( this.handleError );
        let index = 0;
        this.clientes.forEach((item, i) => {
            if(cliente.id == item.id){
                index = i;
            }
        });
        this.clientes.splice(index, 1);
        return Promise.resolve(cliente);
    }

    getById(id: number): Promise<ClienteModel>{
        // return this.getContatos().then(
        //     (contatos: ClienteModel[]) => contatos.find(
        //         (contato) => (cliente.id === id)
        //     )
        // )
        let cliente: ClienteModel = this.clientes.filter((item) => {
            return item.id == id;
        })[0];
        return Promise.resolve(cliente);
    }


    search(term: string): Promise<ClienteModel[]>{
        // return this.http
        // .get(`${this.origin}/?nome=${term}`)
        // .toPromise()
        // .then( (response) => response.json().data as ClienteModel[])
        let clientes = this.clientes.filter((item) => {
            let toSearch = (search:string) => search.toUpperCase().replace(/ /g, '');
            term = toSearch(term);
            return (
                toSearch(item.nome).indexOf(term) !== -1 || 
                toSearch(item.email).indexOf(term) !== -1 || 
                toSearch(item.celular).indexOf(term) !== -1 
            )
        });
        return Promise.resolve(clientes);
    }

}